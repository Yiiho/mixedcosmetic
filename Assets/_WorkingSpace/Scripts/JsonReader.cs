using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;

public class JsonReader : MonoBehaviour {
    public string jsonFileName;
    public string assetsFolder;

    public List<MainData> mainDatas = new List<MainData>();
    public List<SubData> subDatas = new List<SubData>();
    //public List<AdditionData> addDatas = new List<AdditionData>();
    public List<SubData> addDatas = new List<SubData>();

    public float intervalTimer;
    public float gameTime; 
    Global global;
    string assetPath;

    void Start() {
        global = Global.ins;
        ReadAllData();
    }

    void ReadAllData() {
        string dir = Application.dataPath + "/../" + jsonFileName;
        assetPath = Application.dataPath + "/../" + assetsFolder;
        LoadJsonFile(dir);
    }

    private void LoadJsonFile(string path) {
        if (File.Exists(path)) {
            string s = File.ReadAllText(path);
            DoSimpleJson(s);
        } else {
            Debug.Log("File Not Exists at "+path);
        }
    }

     private void DoSimpleJson(string s) {
     //   print(s);
        JSONNode jsonNode = SimpleJSON.JSON.Parse(s);
        //print(jsonNode["Items"].Count+" : Length");

        for (int i = 0; i < jsonNode["Items"].Count; i++) { //For Main
        //for (int i = 0; i < 1; i++) { //For test
            MainData _m = new MainData();
            _m._name = jsonNode["Items"][i]["main"][0];
            _m._data = jsonNode["Items"][i]["main"][1];
            mainDatas.Add(_m);
        //    print("_m._name >> "+ mainDatas[i]._name );
        //    print("_m._data >> "+mainDatas[i]._data);

            for(int j = 0; j < jsonNode["Items"][i]["sub"].Count; j++){ //For sub
                SubData _s = new SubData();
                _s._idItem = i;
                _s._name = jsonNode["Items"][i]["sub"][j][0];
                _s._data = jsonNode["Items"][i]["sub"][j][1];
                _s._dataMin = jsonNode["Items"][i]["sub"][j][2];
               
           //     print("_s._name >> "+  _s._name );
           //     print("_s._data >> "+ _s._data);

                _s._sub = new List<SubInSubData>();
                for(int k = 0; k < jsonNode["Items"][i]["sub"][j][3].Count; k++) { //For In sub
                    SubInSubData _inSub = new SubInSubData();
                    _inSub._name = jsonNode["Items"][i]["sub"][j][3][k][0];
                    _inSub._url = jsonNode["Items"][i]["sub"][j][3][k][1];

                    
                    _inSub._name_new = jsonNode["Items"][i]["sub_new"][j][3][k][0];
                    _inSub._url_new = jsonNode["Items"][i]["sub_new"][j][3][k][1];

            //        print(assetPath + _inSub._url+"              <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                    if(File.Exists(assetPath + _inSub._url)){
                        _inSub._spr = SpriteFromT2D(LoadT2D(assetPath + _inSub._url));
                        _inSub._spr_new = SpriteFromT2D(LoadT2D(assetPath + _inSub._url_new));
                    }   
                    _s._sub.Add(_inSub);

            //        print("_inSub._name >> "+ _inSub._name);
            //       print("_inSub._url >> "+ _inSub._url);
                } // k loop

                subDatas.Add(_s); 

            } // j loop


            /*

            print("l in length = "+jsonNode["Items"][i]["additional"].Count);
            AdditionData _add = new AdditionData();
            List<SubInSubData> _list = new List<SubInSubData>();
            for(int l = 0; l < jsonNode["Items"][i]["additional"].Count; l++) {
                print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+jsonNode["Items"][i]["additional"][l][0]);
             
                SubInSubData _inSub = new SubInSubData();
                _inSub._name = jsonNode["Items"][i]["additional"][l][0];
                _inSub._url = jsonNode["Items"][i]["additional"][l][1];
                if(File.Exists(assetPath + _inSub._url)){
                        _inSub._spr = SpriteFromT2D(LoadT2D(assetPath + _inSub._url));
                    }  
                //if(addData[i]._sub == null) addData[i]._sub = new List<SubInSubData>();
                _list.Add(_inSub);
                //print(addData[i]._sub[l]._name);
            }
            _add._sub = _list;
            addDatas.Add(_add);

            print("addData >>>> "+addDatas[i]._sub[0]._name);
            print("addData >>>> "+addDatas[i]._sub[0]._url);*/

            
            for(int l = 0; l < jsonNode["Items"][i]["additional"].Count; l++){ //For add
                SubData _s = new SubData();
                _s._idItem = i;
                _s._name = jsonNode["Items"][i]["additional"][l][0];
                _s._data = jsonNode["Items"][i]["additional"][l][1];
               
            //    print("_s addd._name >> "+  _s._name );
            //    print("_s addd._data >> "+ _s._data);

                _s._sub = new List<SubInSubData>();
            //    print("                                     addd " + jsonNode["Items"][i]["additional"][l][2].Count);
                for (int m = 0; m < jsonNode["Items"][i]["additional"][l][2].Count; m++) { //For In add
                    SubInSubData _inSub = new SubInSubData();
                    _inSub._name = jsonNode["Items"][i]["additional"][l][2][m][0];
                    _inSub._url = jsonNode["Items"][i]["additional"][l][2][m][1];

                    
                    _inSub._name_new = jsonNode["Items"][i]["additional_new"][l][2][m][0];
                    _inSub._url_new = jsonNode["Items"][i]["additional_new"][l][2][m][1];

                //    print(assetPath + _inSub._url+"              <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                    if(File.Exists(assetPath + _inSub._url)){
                        _inSub._spr = SpriteFromT2D(LoadT2D(assetPath + _inSub._url));
                        _inSub._spr_new = SpriteFromT2D(LoadT2D(assetPath + _inSub._url_new));
                    }   
                    _s._sub.Add(_inSub);

                //    print("_inSub addd._name >> "+ _inSub._name);
                //    print("_inSub addd._url >> "+ _inSub._url);
                } // m loop

                addDatas.Add(_s); 

            } // l loop

        } // i loop
          
        intervalTimer = float.Parse(SplitString(jsonNode["IntervalTime"].ToString()));
        gameTime = float.Parse(SplitString(jsonNode["GameTime"].ToString()));

        OnReadJsonCompleted();
        
    }

    void OnReadJsonCompleted() {        
     //   global.listDatas = listDatas;
        global.intervalTimer = intervalTimer;
        global.gameTime = gameTime;

        global.LoadSceneAt(1);

       // global.LoadSceneAt(3);
    }

    public static Sprite SpriteFromT2D(Texture2D tex){
        return Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
    }

    public static Texture2D LoadT2D(string filePath) {
        Texture2D tex = null;
        byte[] fileData;

        fileData = File.ReadAllBytes(filePath);
        tex = new Texture2D(2,2);
        tex.LoadImage(fileData);

        return tex;
    }

    string SplitString(string s) {
        s = s.Replace(@"\", "/");
        s = s.Replace("\"", "");
        return s;
    }

}

[System.Serializable]
public struct MainData {
    public string _name;
    public string _data;
}

[System.Serializable]
public struct SubData {   
    public string _name;
    public string _data;
    public string _dataMin;
    public int _idItem;
    public List<SubInSubData> _sub;
}

[System.Serializable]
public struct AdditionData {
    public List<SubInSubData> _sub;
}

[System.Serializable]
public struct SubInSubData {
    public Sprite _spr;
    public Sprite _spr_new;
    public string _name;
    public string _url;
    public string _name_new;
    public string _url_new;
}
