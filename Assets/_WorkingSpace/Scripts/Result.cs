﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class Result : MonoBehaviour {
    MainData mainData;
    List<SubData> additionData;
    List<SubData> subItemData;
    List<int> subSelectedIndex;
    public List<List<bool>> addiSelectedIndex;
    public Image itemShow;
    public TextMeshProUGUI headerProduct, addiText;

    public Transform subItemContainer;
    public GameObject subItemObj;

   // public ResultItemCustomColor iCustomColor;
    public Transform addiItemContainer;
    Global global;
    void Start() {
        global = Global.ins;
        /*
        if (global.itemSelector < 4) {
            print(" < 4");
            iCustomColor.SetColorByIndex(global.itemSelector);
        } else {
            print(" >= 4 ");
            itemShow.sprite = global.itemBases[global.itemSelector];
            itemShow.SetNativeSize();
        }*/
        mainData = global.GetMainItemSelectData();
        subItemData = global.GetSubItemSelectData();
        additionData = global.GetAddiItemSelectData();
        subSelectedIndex = global.subSelectedIndex;
        addiSelectedIndex = global.addiSelectedIndex;
        headerProduct.text = mainData._name;

        for(int i = 0; i < subItemData.Count; i++){
            GameObject child = Instantiate (subItemObj, Vector3.zero , Quaternion.identity);
            child.transform.SetParent(subItemContainer);
            SubData _s = subItemData[i];
            child.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = _s._sub[subSelectedIndex[i]]._name;
            child.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = _s._dataMin;
            child.transform.localScale = new Vector3(1,1,1);
        }
        addiText.text = "";
        for(int j = 0; j < addiSelectedIndex.Count; j++){
            for (int k = 0; k < addiSelectedIndex[j].Count; k++) {
                print(additionData[j]._sub[k]._name);
                addiText.text += (addiSelectedIndex[j][k] == true) ? additionData[j]._sub[k]._name + "<br>" : "";
            }
        }
    }

    public void OnClickNext(){
        Global.ins.PlayClickSound();
        if (subItemContainer.gameObject.activeSelf) {
            subItemContainer.gameObject.SetActive(false);
            addiItemContainer.gameObject.SetActive(true);
        } else {
            addiItemContainer.gameObject.SetActive(false);
            global.LoadSceneAt(7);
        }
    }

}
