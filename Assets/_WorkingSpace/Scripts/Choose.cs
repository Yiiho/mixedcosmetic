﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class Choose : MonoBehaviour {
    public GameObject[] allButton;
    public GameObject[] allLine;
    public Image logoBG, logoName;
    Global global ;

    void Start() {
        global = Global.ins;
    }

    public void OnSelectId(int id) {
        Global.ins.PlayClickSound();
        float delayTimer = 0.1f;
        GameObject go = new GameObject();
        global.itemSelector = id;
        global.SetMainItemSelect(id);
        global.SetSubItemSelect(id);
        global.SetAddiItemSelect(id);

        for(int i = 0; i < allButton.Length; i++){            
            if(i == id) {
                go = allButton[i].gameObject;
                allButton[i].GetComponent<RectTransform>().SetSiblingIndex(allButton.Length-1);
            } else {
                int _i = i;
                allButton[_i].GetComponent<Image>().DOFade(0f, .3f).SetDelay(delayTimer).OnComplete(()=>
                    allButton[_i].SetActive(false));
                delayTimer += 0.1f;
            }
            Destroy( allButton[i].gameObject.GetComponent<Button>() );
        }
        foreach(GameObject _go in allLine){
            _go.SetActive(false);
        }
        logoBG.gameObject.SetActive(false);
        logoName.gameObject.SetActive(false);
        go.transform.DOLocalMove(new Vector2(0, 0), .5f).SetDelay(delayTimer).OnComplete(()=>{
            allButton[id].GetComponent<Image>().DOFade(0f, .5f);
            allButton[id].transform.DOScale(new Vector2(9,9), .5f).OnComplete(LoadNextScene);
        });    
    }

    void LoadNextScene() {
        Global.ins.LoadSceneAt(4);
    }

}
