﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;

public class ShowData : MonoBehaviour {
    Global global;
    //StructData data;
    int currentPage;
    public Image itemShow;
    public TextMeshProUGUI txtTopic,  txtHeader, txtData, txtPage;
    public Image imgItem, memo, bgAll;
    MainData mainData;
    List<SubData> subItemData;
    void Start() {
        global = Global.ins;
        itemShow.sprite = global.itemBases[global.itemSelector];
        itemShow.SetNativeSize();

        mainData = global.GetMainItemSelectData();
        txtTopic.text = mainData._name;
        txtHeader.text = "";
        txtData.text = mainData._data;
       
        subItemData = global.GetSubItemSelectData();

        currentPage = 0;

        txtPage.text = (currentPage + 1) + "/" + (subItemData.Count+1);
    }

    public void OnPress() {
        txtTopic.text = "ส่วนผสมหลักของ<br>" + mainData._name; 
        Global.ins.PlayClickSound();
        if(currentPage>=subItemData.Count){
            OnEndScene();
        } else{
            itemShow.sprite = subItemData[currentPage]._sub[0]._spr;
            itemShow.SetNativeSize();
            txtHeader.text = subItemData[currentPage]._name;
            txtData.text = subItemData[currentPage]._data;
            currentPage++;
            txtPage.text = (currentPage + 1) + "/" + (subItemData.Count+1);
        }
    }

    private void OnEndScene() {
        memo.transform.DOScaleY(0, .5f).SetDelay(.2f);
        imgItem.DOFade(0, 1.5f);
        imgItem.transform.DOScale(Vector3.zero, 1.5f);
        imgItem.transform.DOJump(Vector3.zero, 5, 1, 1.5f);
        bgAll.transform.DOScaleX(10, 1).SetDelay(1.2f);
        bgAll.DOFade(0, 1).SetDelay(1.2f);
        txtHeader.DOFade(0, .2f).SetDelay(.2f);
        txtData.DOFade(0, .2f).SetDelay(.1f);
        txtPage.DOFade(0, .2f).SetDelay(.1f);

        DOVirtual.DelayedCall(1.7f, ()=> global.LoadSceneAt(5));
    }
}
