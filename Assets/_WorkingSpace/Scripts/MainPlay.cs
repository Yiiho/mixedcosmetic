﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainPlay : MonoBehaviour {
    public Transform baseAll, G1, G2, G3;
    public float baseTweenTime = .25f;
    Vector2 oriPos, oriScl;
    Quaternion oriRot;
    public ImageGroup[] chemicalBaseImage;
    public PosAndRot[] basePosRot;
    int currentSelector = -1;
    int[] chemicalSelected = new int[3];
    bool isOnMix = false;

    void Start() {
        oriPos = baseAll.localPosition;
        oriRot = baseAll.rotation;
        oriScl = baseAll.localScale;
        for (int i = 0; i< chemicalSelected.Length; i++) {
            chemicalSelected[i] = -1;
        }
        ActiveButtonGroup(0, false);
        ActiveButtonGroup(1, false);
        ActiveButtonGroup(2, false);
    }

    void ActiveButtonGroup(int idGroup, bool isActive) {
        for (int i = 0; i < chemicalBaseImage[idGroup].groupChemicalImage.Length; i++) {
            if (isActive) {
                chemicalBaseImage[idGroup].groupChemicalImage[i].gameObject.SetActive(true);
                chemicalBaseImage[idGroup].groupChemicalImage[i].DOFade(1f, .3f).SetDelay(.3f + ((float)i / 2));
            } else {
                chemicalBaseImage[idGroup].groupChemicalImage[i].DOFade(0f, 0f);
                chemicalBaseImage[idGroup].groupChemicalImage[i].gameObject.SetActive(false);
            }
        }
        
    }

    public void SetBaseAll(int id) {
        if (isOnMix) return;
        if(currentSelector == -1) {
            baseAll.DOLocalMove(basePosRot[id].pos, baseTweenTime)
                .OnComplete(()=>currentSelector = id);
            baseAll.DORotate(basePosRot[id].rot, baseTweenTime);
            baseAll.DOScale(basePosRot[id].scl, baseTweenTime);
            ActiveButtonGroup(id, true);
        } else {
            ResetBase();
            ActiveButtonGroup(id, false);
        }
    }

    public void ResetBase() {
        if (isOnMix) return;
        baseAll.DOLocalMove(oriPos, baseTweenTime)
            .OnComplete( () => currentSelector = -1);
        baseAll.DORotateQuaternion(oriRot, baseTweenTime);
        baseAll.DOScale(oriScl, baseTweenTime);
    }

    public void SetChemical_0(int id) {
        chemicalSelected[0] = id;
        chemicalBaseImage[0].baseChemicalimage.sprite =
            chemicalBaseImage[0].groupChemicalImage[id].sprite;
        
    }
    public void SetChemical_1(int id) {
        chemicalSelected[1] = id;
        chemicalBaseImage[1].baseChemicalimage.sprite =
            chemicalBaseImage[1].groupChemicalImage[id].sprite;
    }
    public void SetChemical_2(int id) {
        chemicalSelected[2] = id;
        chemicalBaseImage[2].baseChemicalimage.sprite =
            chemicalBaseImage[2].groupChemicalImage[id].sprite;
    }

    public void MixAll() {
        if (currentSelector == -1) return;
        foreach(int i in chemicalSelected) {
            if (i == -1) return;
        }

        //anim..............
    }
}

[System.Serializable]
public struct PosAndRot {
    public Vector2 pos;
    public Vector3 rot;
    public Vector2 scl;
}

[System.Serializable]
public struct ImageGroup {
    public Image baseChemicalimage;
    public Image[] groupChemicalImage;
}