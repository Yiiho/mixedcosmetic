using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultItemCustomColor : MonoBehaviour
{
    public Image itemShow, itemShow1, itemShow2;
    public string interuptCase;
    public ImageLayer[] imageLayers;

    int mainIndex;

    // Start is called before the first frame update
    void Start() {
        mainIndex = Global.ins.itemSelector;
        itemShow.sprite = imageLayers[mainIndex].imgs[0];
        itemShow.SetNativeSize();
        if (imageLayers[mainIndex].imgs.Length == 1) {
            itemShow1.gameObject.SetActive(false);
            itemShow2.gameObject.SetActive(false);
        } else {
         //   itemShow1.gameObject.SetActive(true);
         //   itemShow2.gameObject.SetActive(true);
            itemShow1.sprite = imageLayers[mainIndex].imgs[1];
            itemShow1.color = GetColorValue();
            itemShow1.SetNativeSize();
            itemShow2.sprite = imageLayers[mainIndex].imgs[2];
            itemShow2.SetNativeSize();
        }
    }

    Color GetColorValue() {
        List<SubData> subItemData = Global.ins.GetSubItemSelectData();
        List<int> subSelectedIndex = Global.ins.subSelectedIndex;

        for (int i = 0; i < subItemData.Count; i++) {
            if(subItemData[i]._name == interuptCase) {
                return Global.ins.GetColorByName(subItemData[i]._sub[subSelectedIndex[i]]._name);
            }
        //    print("at i = " + i + "; data name == " + subItemData[i]._name);
        //    print("at i = " + i + "; data name == " + subItemData[i]._sub[subSelectedIndex[i]]._name);
        }
        return Color.white;
    }
    
}

[System.Serializable]
public struct ImageLayer {
    public Sprite[] imgs;
}