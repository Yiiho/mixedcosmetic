﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemDragHandle : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    public delegate void OnEndDragDispatcher(ItemDragHandle iDrag);
    public event OnEndDragDispatcher OnDrop;
    Vector2 originPosition;
    Collider collider;
    Play play;
    void Start() {
        originPosition = transform.localPosition;
        collider = GetComponent<Collider>();
        play = GameObject.FindObjectOfType<Play>();
    }

    public void SetData(Sprite sp, string s, int indexMixer) {
        GetComponent<Image>().sprite = ItemImage = sp;
        ItemData = s;
        GetIndexOfMixer = indexMixer;
    }

    public int GetIndexOfMixer {
        get; private set;
    }

    public void OnBeginDrag(PointerEventData eventData){
        play.bookImage.sprite = ItemImage;
        play.bookData.text = ItemData;
    }

    public void OnDrag(PointerEventData eventData) {
        transform.position = Input.mousePosition;
        transform.SetSiblingIndex(8);
    }

    public void OnEndDrag(PointerEventData eventData) {
        if(OnDrop!= null) OnDrop(this);
    }

    public void ResetPosition() {
        transform.localPosition = originPosition;
    }

    public Sprite ItemImage {
        get; private set;
    }
    public string ItemData {
        get; private set;
    }

    public Collider GetCollider() {
        return collider;
    }
}
