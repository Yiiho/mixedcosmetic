﻿using UnityEngine;

public class LastScene : MonoBehaviour {

    public void OnClickReplay() {
        Global.ins.LoadSceneAt(1);
        Global.ins.PlayClickSound();
    }
}
