﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MockData : MonoBehaviour {
    public MockDatas[] datas;
    public MockDatas[] additionals;
        
}

[System.Serializable]
public struct MockDatas {
    public Sprite sp;
    public string data;
    public int index;
}

