﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class Global : MonoBehaviour {
    public static Global ins;
    public JsonReader jsonReader;

    //public List<StructData> listDatas = new List<StructData>();

    public Sprite[] itemBases;
    public float intervalTimer;
    public float gameTime;

    public Sprite[] mainSpr = new Sprite[3];
    public string[] mainData = new string[3];
    public Sprite[] subSpr = new Sprite[3];
    public string[] subData = new string[3];

    public AudioSource audioSource;

    public List<NameAndColor> nameAndColors;

    float timer = 0;
    void Awake() {
        if(ins == null) ins = this;
        DontDestroyOnLoad(this.gameObject);
    }

    void Start() {
        Cursor.visible = false;
        Invoke("SetFullScreen", 2.5f);
    }

    void SetFullScreen() {
        Screen.fullScreen = true;
    }

    public void LoadSceneAt(int id){
        SceneManager.LoadScene(id);
    }

    public int itemSelector { get; set; }

    public bool PlayerResult { get; set; }

    public List<int> subSelectedIndex { get; set; }

    public List<List<bool>> addiSelectedIndex { get; set;}

    public void PlayClickSound(){
        audioSource.Play();
    }
    private void Update() {
        timer += Time.deltaTime;

        if(Input.GetMouseButton(0)){
            timer = 0;
        }
        if(timer >= gameTime){
            timer = 0;
            LoadSceneAt(1);
        }
        if (Input.GetKeyUp(KeyCode.M)) {
            Cursor.visible = !Cursor.visible;
        }
    }


#region Main Item Selected
    MainData mainItemData;
    public void SetMainItemSelect(int id){
        mainItemData = jsonReader.mainDatas[id];
    }

    public MainData GetMainItemSelectData() {
        return mainItemData;
    }
#endregion

#region Sub-Item Selected
    List<SubData> subItemData;
    public void SetSubItemSelect(int id){
        if(subItemData!=null) subItemData = null;
        subItemData = new List<SubData>();
        foreach(SubData sd in jsonReader.subDatas){
            if(sd._idItem == id){
                subItemData.Add(sd);
            }
        }
    }
    public List<SubData> GetSubItemSelectData(){
        return subItemData;
    }
#endregion

#region Additional Item Selected
/*
    AdditionData addiItemData;
    public void SetAddiItemSelect(int id){
   //     addiItemData = jsonReader.addDatas[id];
    }
    public AdditionData GetAddiItemSelectData(){
        return addiItemData;
    }
*/
    List<SubData> addiItemData;
    public void SetAddiItemSelect(int id){
        if(addiItemData!=null) addiItemData = null;
        addiItemData = new List<SubData>();
        foreach(SubData sd in jsonReader.addDatas){
            if(sd._idItem == id){
                addiItemData.Add(sd);
            }
        }
    }
    public List<SubData> GetAddiItemSelectData(){
        return addiItemData;
    }

#endregion

    public Color GetColorByName(string _name) {
        for (int i = 0; i < nameAndColors.Count; i++) {
            if(nameAndColors[i].cName == _name) {
                return nameAndColors[i].color;
            }
        }
        return nameAndColors[0].color;
    }
}

[System.Serializable]
public struct NameAndColor {
    public string cName;
    public Color color;
}
