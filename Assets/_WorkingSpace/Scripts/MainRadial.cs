﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
public class MainRadial : MonoBehaviour {
    public GameObject mainGroup;
    public MapButtonAndPosition[] mainButtonGroup;
    public GameObject radialGroup, mixGroup, jobGroup;
    public Image nextButton;
    public Image mixButton;
    public Image caution;
    Text cautionTxt;
    Button[] radialButtons, ingradiantButtons;
    int[] ingradaintPull = new int[3];
    bool isMixed;

    void Start() {
        isMixed = false;
        radialButtons = radialGroup.transform.GetComponentsInChildren<Button>();
        ingradiantButtons = mixGroup.transform.GetComponentsInChildren<Button>();
        Image[] imgs = mixGroup.transform.GetComponentsInChildren<Image>();
        cautionTxt = caution.GetComponentInChildren<Text>();
        caution.gameObject.SetActive(false);
        ActiveRadial(false, false);
        for (int i = 0; i < ingradaintPull.Length; i++) {
            ingradaintPull[i] = -1;
        }
        foreach(Image img in imgs) {
            img.color = new Color(img.color.r, img.color.g, img.color.b, 0);
        }
        OnActiveMixButton();
    }

    public void FocusGroup(int id) {
        for(int i = 0; i < mainButtonGroup.Length; i++) {
            if (id == i) {
                mainButtonGroup[i].btn.transform.DOLocalMove(mainButtonGroup[i].pos, .35f)
                    .OnComplete(()=> ActiveRadial(true));
            } else {
                mainButtonGroup[i].btn.image.DOFade(0, .15f);
            }
        }
    }

    private void ActiveRadial(bool isActive, bool isFade=true) {
        if (isActive) {
            radialGroup.SetActive(true);
            for(int i = 0; i < radialButtons.Length; i++) {
                radialButtons[i].image.DOFade(1f, .55f).SetEase(Ease.InBack).SetDelay((float)i / 10);
            }
            mixGroup.SetActive(true);
            foreach(Button btn in ingradiantButtons) {
                btn.image.DOFade(1f, 1f);
            }
        } else {
            if (isFade) { //end program process.
                for (int i = 0; i < radialButtons.Length; i++) {
                    if (i != radialButtons.Length - 1) {
                        radialButtons[i].image.DOFade(0f, .55f).SetEase(Ease.InBack).SetDelay((float)i / 10);
                    } else {
                        radialButtons[i].image.DOFade(0f, .55f).SetEase(Ease.InBack).SetDelay((float)i / 10).OnComplete(ShowResult);
                    }
                }
            } else { //init process.
                mixGroup.SetActive(false);
                foreach (Button btn in radialButtons) {
                    btn.image.DOFade(0f, 0f);
                }
                radialGroup.SetActive(false);
            }
        }
    }

    private void ShowResult() {
        radialGroup.SetActive(false);
        float twTimer = 0.6f;
        mixGroup.transform.DOLocalMove(new Vector2(-488, -46.5f), twTimer);
        mixGroup.transform.DOScale(new Vector2(1.5f, 1.5f), twTimer);
        mainGroup.transform.DOLocalMove(new Vector2(-488, -42f), twTimer);
        mainGroup.transform.DOScale(new Vector2(1.5f, 1.5f), twTimer);

        DoJobGroup(twTimer);        
    }

    private void DoJobGroup(float twTimer) {
        Image img = jobGroup.transform.Find("Bubble").GetComponent<Image>();
        img.color = new Color(img.color.r, img.color.g, img.color.b, 0);
        jobGroup.SetActive(true);
        
        jobGroup.transform.DOLocalMove(new Vector2(0, 0), twTimer).OnComplete(()=>
            img.DOFade(1, .25f).OnComplete(ShowNextButtton));
    }

    private void ShowNextButtton() {
        nextButton.gameObject.SetActive(true);
        nextButton.DOFade(1, 1f).SetDelay(.5f);
    }

    public void OnPressMix() {
        if (isMixed) return;
        for (int i = 0; i < ingradaintPull.Length; i++) {
            if (ingradaintPull[i] == -1) {
                Noti("กรุณาใส่ส่วนผสมให้ครบจำนวน");
                return;
            }
        }
        isMixed = true;
        ActiveRadial(false);
        mixButton.DOFade(0, .3f).OnComplete(() => mixButton.gameObject.SetActive(false));
    }

    public void OnAddIngradiant(int id) {
        if (isMixed) return;
        foreach (int _i in ingradaintPull) {
            if (_i == id) {
                Noti("ส่วนผสมนี้ถูกใช้ไปแล้ว");
                return;
            }
        }
        for(int i = 0; i < ingradaintPull.Length; i++) {
            if (ingradaintPull[i] == -1) {
                ingradaintPull[i] = id;
                ingradiantButtons[i].image.sprite = radialButtons[id].image.sprite;
                radialButtons[id].image.DOFade(.75f, .15f);
                OnActiveMixButton();
                return;
            }
        }
        
        Noti("ส่วนผสมเต็มแล้ว กรุณาลบส่วนผสมเดิมก่อน");
    }

    public void OnPressIngradiant(int id) {
        if (isMixed) return;
        if (ingradaintPull[id] != -1) {
            radialButtons[ingradaintPull[id]].image.DOFade(1f, .15f);
            ingradaintPull[id] = -1;
            ingradiantButtons[id].image.sprite = null;
            mixButton.DOFade(.75f, .1f);
        }
    }

    private void OnActiveMixButton() {
        foreach (int _i in ingradaintPull) {
            if (_i == -1) {
                mixButton.DOFade(.75f, .1f);
                return;
            }
        }
        mixButton.DOFade(1f, .1f);
    }

    private void Noti(string s) {
        cautionTxt.text = s;
        caution.transform.localPosition = new Vector2(0, 150);
        caution.gameObject.SetActive(true);
        caution.transform.DOLocalMoveY(0, 0.3f).OnComplete(() =>
            caution.transform.DOLocalMoveY(150, 0.3f).OnComplete(() =>
                caution.gameObject.SetActive(false)).SetDelay(1.5f));
    }

    public void OnPressNext() {
        SceneManager.LoadScene(0);
    }

}

[Serializable]
public struct MapButtonAndPosition {
    public Button btn;
    public Vector2 pos;
}
