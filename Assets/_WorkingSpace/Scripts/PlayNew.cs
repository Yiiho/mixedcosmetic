using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;

public class PlayNew : MonoBehaviour {
    //MainData mainData;
    List<SubData> additionData;
    List<SubData> subItemData;
    public Transform drAndBg;
    public Transform rightBG;
    public TextMeshProUGUI textMixer;
    public Image fade;
    public Image[] imgsItemSeleted;
    public Sprite[] selectorBG;
    public GameObject showAdditionalGroupButton;
    public Button nextButton;
    public GameObject scrollViewObj;
    Global global;

    public Image rightPanelContentSlider;
    public GameObject rightButtonPrefab;
    public GameObject timeline1, timeline2;

    public GameObject instantItemButton;
    public Sprite[] panelBg;

    public GameObject slideGuideObj;

//sub panel
    List<string> nameInSubDatas;
    List<SubInSubData> inSubDatas;
    List<int> subSelectedIndex;
///////////////

//additional panel
    List<List<bool>> addiSelectedIndex;
    List<Sprite> addiItemOnTray;
//////////////////
    bool isSelectAddtional;

    private void Awake() {
        DOTween.KillAll();
    }
    void Start() {
        isSelectAddtional = false;
        global = Global.ins;
        subItemData = global.GetSubItemSelectData();
        additionData = global.GetAddiItemSelectData();
        subItemLayer.SetActive(false);
        addiLayer.SetActive(false);
        SetRightPanelContentButton(subItemData.Count);
        foreach(Image img in imgsItemSeleted){
            img.color=new Color(1,1,1,0);
        }
        textMixer.text = "ส่วนผสมหลัก";
        addiSelectedIndex = new List<List<bool>>();
    }
#region  SubItemPanel 
    void SetRightPanelContentButton(int amount) {
        nameInSubDatas = new List<string>();
        subSelectedIndex = new List<int>();
        slideGuideObj.SetActive(true);
        switch (amount){
            case 6:
                rightPanelContentSlider.rectTransform.sizeDelta = new Vector2(0, 1600);
                break;
            case 5:
                rightPanelContentSlider.rectTransform.sizeDelta = new Vector2(0, 1350);
                break;
            case 4:
                rightPanelContentSlider.rectTransform.sizeDelta = new Vector2(0, 1050);
                break;
            default:
                slideGuideObj.SetActive(false);
                rightPanelContentSlider.rectTransform.sizeDelta = new Vector2(0, 850);
                break;
        }
        for(int i = 0; i < amount; i++) {
            subSelectedIndex.Add(-1);
            nameInSubDatas.Add(subItemData[i]._name);
            GameObject child = Instantiate (rightButtonPrefab, Vector3.zero , Quaternion.identity);
            child.transform.SetParent(rightPanelContentSlider.gameObject.transform);
            int id = i;
            child.GetComponent<Button>().onClick.AddListener(()=>OnSelectSubItemByIndex(id, child));
            Image img = child.transform.GetChild(0).GetComponent<Image>();
            img.sprite = subItemData[i]._sub[0]._spr;
        }
    }

    public void ToggleTimeline(){
        Global.ins.PlayClickSound();
        timeline1.SetActive(false);
        timeline2.SetActive(true);
    }
    public GameObject subItemLayer;
    public Image subItemBG;
    //public Image[] subItemButtons;
    public TextMeshProUGUI subItemName;
    GameObject container;
    List<Image> containerImgs;
    GameObject dummyGO;
    void OnSelectSubItemByIndex(int id, GameObject _go) {
        dummyGO = _go;
        containerImgs = new List<Image>();
        container = subItemLayer.transform.Find("ButtonContainer").gameObject;
        foreach (Transform child in container.transform) { //remove all BTN previous.
            GameObject.Destroy(child.gameObject);
        }
        if (subItemData[id]._sub.Count > 4) {
            subItemBG.sprite = panelBg[1];
            subItemBG.SetNativeSize();
            if (subItemData[id]._sub.Count >= 6) { // Reduce space for 6 items.
                container.GetComponent<GridLayoutGroup>().spacing = new Vector2(-40, 0);
            }
        } else {
            subItemBG.sprite = panelBg[0];
            subItemBG.SetNativeSize();
        }
        for (int i = 0; i < subItemData[id]._sub.Count; i++) { //Create new group of button
            GameObject go = Instantiate(instantItemButton);
            go.transform.SetParent(container.transform);
            Button _B = go.GetComponent<Button>();
            // _B.onClick.RemoveAllListeners();
            containerImgs.Add(_B.image);
            if (subSelectedIndex[id] != i) {
                _B.image.sprite = selectorBG[0];
            } else {
                _B.image.sprite = selectorBG[1];
            }

/*
            _B.transform.GetChild(0).GetComponent<Image>().sprite = subItemData[id]._sub[i]._spr;
            _B.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = subItemData[id]._sub[i]._name;
*/
            _B.transform.GetChild(0).GetComponent<Image>().sprite = subItemData[id]._sub[i]._spr_new;
            _B.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "";//subItemData[id]._sub[i]._name;

            int _id = i;
            _B.onClick.AddListener(()=>OnSelectSubItemInSub(id,_id));
        }
        Global.ins.PlayClickSound();
        subItemLayer.SetActive(true);
        subItemName.text = nameInSubDatas[id];
        /*
        if (subSelectedIndex[id]!=-1)
            container.transform.GetChild(id).GetComponent<Image>().sprite = selectorBG[1];*/
    }

    void  OnSelectSubItemInSub(int subIndex, int subInSubIndex) {
        Global.ins.PlayClickSound();
        subSelectedIndex[subIndex] = subInSubIndex;
        for(int i = 0; i < containerImgs.Count; i++){
          //  print(subInSubIndex + "            =====             " + i);
            if(i!=subInSubIndex){
                containerImgs[i].sprite = selectorBG[0];
            }else{
                containerImgs[i].sprite = selectorBG[1];
            }
        }
        imgsItemSeleted[subIndex].color = Color.white;
        imgsItemSeleted[subIndex].sprite = subItemData[subIndex]._sub[subInSubIndex]._spr;
        dummyGO.GetComponent<Image>().color = new Color(1, 1, 1, .6f);
    }

    public void OnCloseSubItemLayer() {
        Global.ins.PlayClickSound();
        subItemLayer.SetActive(false);
        CheckSubItemIsFullSelected();
    }
     void CheckSubItemIsFullSelected() {
        if(!subSelectedIndex.Contains(-1)){
            nextButton.gameObject.SetActive(true);
        }
    }
#endregion

#region Additional panel
    public GameObject addiLayer;
    public Image addiLayerBG;
    public TextMeshProUGUI addiNameText;

    void SetRightPanelAdditional(){
        textMixer.text = "ส่วนผสมเสริม";
        containerImgs = new List<Image>();
        foreach (Transform child in rightPanelContentSlider.gameObject.transform) {
            GameObject.Destroy(child.gameObject);
        } 
        nameInSubDatas = new List<string>();
        inSubDatas = new List<SubInSubData>();

        slideGuideObj.SetActive(true);
        switch (additionData.Count){
            case 6:                
                rightPanelContentSlider.rectTransform.sizeDelta = new Vector2(0, 1600);
                break;
            case 5:
                rightPanelContentSlider.rectTransform.sizeDelta = new Vector2(0, 1350);
                break;
            case 4:
                rightPanelContentSlider.rectTransform.sizeDelta = new Vector2(0, 1050);
                break;
            default:
                slideGuideObj.SetActive(false);
                rightPanelContentSlider.rectTransform.sizeDelta = new Vector2(0, 850);
                break;
        }
        for(int i = 0; i < additionData.Count; i++) {
            List<bool> dumList = new List<bool>();
            for(int j= 0; j < additionData[i]._sub.Count; j++){
                dumList.Add(false);
            }
            addiSelectedIndex.Add(dumList);
            
            int id = i;
            nameInSubDatas.Add(additionData[id]._name);
            GameObject child = Instantiate (rightButtonPrefab, Vector3.zero , Quaternion.identity);
            child.transform.SetParent(rightPanelContentSlider.transform);
            child.GetComponent<Button>().onClick.AddListener(()=> OnOpenAdditionalPanel(id));
            Image img = child.transform.GetChild(0).GetComponent<Image>();
            img.sprite = additionData[id]._sub[0]._spr;
        }
    }

    public void OnOpenAdditionalPanel(int id) { //Show addi layer
        containerImgs = new List<Image>();
        container = addiLayer.transform.Find("ButtonContainer").gameObject;
        tmbSelectedList = null;
        tmbSelectedList = new List<bool>();
        addiItemOnTray = new List<Sprite>();
        tmbSeletedID = id;
        foreach (Transform child in container.transform) { //remove all BTN previous.
            GameObject.Destroy(child.gameObject);
        }
        if (additionData[id]._sub.Count > 4) {
            addiLayerBG.sprite = panelBg[1];
            addiLayerBG.SetNativeSize();
            if (additionData[id]._sub.Count >= 6) { // Reduce space for 6 items.
                container.GetComponent<GridLayoutGroup>().spacing = new Vector2(60, 0);
            }
        } else {
            addiLayerBG.sprite = panelBg[0];
            addiLayerBG.SetNativeSize();
        }

        for (int i = 0; i < additionData[id]._sub.Count; i++) { //Create new group of button
            GameObject go = Instantiate(instantItemButton);
            go.transform.SetParent(container.transform);
            Button _B = go.GetComponent<Button>();
            containerImgs.Add(_B.image);
            _B.image.sprite = selectorBG[0];
 /*
            _B.transform.GetChild(0).GetComponent<Image>().sprite = additionData[id]._sub[i]._spr;
            _B.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = additionData[id]._sub[i]._name;
*/
            
            _B.transform.GetChild(0).GetComponent<Image>().sprite = additionData[id]._sub[i]._spr_new;
            _B.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "";//additionData[id]._sub[i]._name_new;
            addiItemOnTray.Add(additionData[id]._sub[i]._spr);


            int _id = i;            
            if (addiSelectedIndex[id][i] == true) {
                containerImgs[i].sprite = selectorBG[1];
            } else {
                containerImgs[i].sprite = selectorBG[0];
            }
            _B.onClick.AddListener(() => OnSelectAdditional(id, _id, !tmbSelectedList[_id]));
            tmbSelectedList.Add(addiSelectedIndex[id][i]);
        }

        Global.ins.PlayClickSound();
        addiLayer.SetActive(true);
        addiNameText.text = nameInSubDatas[id];
    }

    void OnSelectAdditional(int _mainIndex, int _subIndex, bool b){
        tmbSeletedID = _mainIndex;
        if (addiSelectedIndex[_mainIndex][_subIndex] == true)
            container.transform.GetChild(_subIndex).GetComponent<Image>().sprite = selectorBG[1];
        tmbSelectedList[_subIndex] = b;

        Global.ins.PlayClickSound();
        //containerImgs[_subIndex].sprite = selectorBG[Convert.ToInt32( tmbSelectedList[_subIndex])];
        
        for (int i = 0; i < tmbSelectedList.Count; i++) {
            if(tmbSelectedList[i] == false){     
                containerImgs[i].sprite = selectorBG[0];
            } else {
                containerImgs[i].sprite = selectorBG[1];
            }
        }
    }

    List<bool> tmbSelectedList; //check additional selected.
    int tmbSeletedID;
    

    public void OnCloseAdditionalPanel(bool isSave){
        Global.ins.PlayClickSound();
        addiLayer.SetActive(false);
        nextButton.gameObject.SetActive(true);
        if (isSave) {
            addiSelectedIndex[tmbSeletedID] = tmbSelectedList;
            int index = 3 * tmbSeletedID; 
            for (int i = 0; i < tmbSelectedList.Count; i++) {
                imgsItemSeleted[index + i].sprite = addiItemOnTray[i];
                if (tmbSelectedList[i] == true) {
                    imgsItemSeleted[index + i].color = Color.white;
                } else {
                    imgsItemSeleted[index + i].color = new Color(1, 1, 1, 0);
                }
            }
        }

        /*
        int index = 0;
        for (int i = 0; i < addiSelectedIndex.Count; i++) {
            for (int j = 0; j < addiSelectedIndex[i].Count; j++) {
                index = (((i + 1) * (j + 1)) - 1);
                if (addiSelectedIndex[i][j] == false) {
                    imgsItemSeleted[index].color = new Color(1, 1, 1, 0);
                } else {
                    imgsItemSeleted[index].sprite = additionData[i]._sub[j]._spr;
                    imgsItemSeleted[index].color = new Color(1,1,1,1);
                }
            }
        }*/


       // additionData[id]._sub[i]._spr
    }

#endregion

    public void OnClickNextButton() {
        Global.ins.PlayClickSound();
        nextButton.gameObject.SetActive(false);  
        if(!isSelectAddtional){
            isSelectAddtional = true;
            SetRightPanelAdditional();
        }else{
            float t = 2.5f;
            global.subSelectedIndex = subSelectedIndex;
            global.addiSelectedIndex = addiSelectedIndex;
            rightBG.transform.DOLocalMoveX(2000, .5f);
            DOVirtual.DelayedCall(t,()=> global.LoadSceneAt(6));
            drAndBg.DOScale(new Vector2(1.5f, 1.5f), t).SetEase(Ease.InBack);
            fade.DOFade(1, t);
        }
        AnimItemToBowl();
    }

    void AnimItemToBowl(){
        foreach(Image img in imgsItemSeleted) {
            Vector3 tmpPos = img.transform.position;
            img.transform.DOJump(new Vector3(-1.2f,-1.5f,0), 1.5f, 1, .5f, false).OnComplete(()=> img.transform.position = tmpPos);
            img.DOFade(0, .5f);
        }
    }

}
