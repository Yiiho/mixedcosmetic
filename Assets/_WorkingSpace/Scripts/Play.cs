﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class Play : MonoBehaviour {
    public GameObject bowl;
    public GameObject[] bowlLockSlot;
    ItemDragHandle[] bowDataSlot = new ItemDragHandle[3];
    public Image[] ingradiants;
    
    public Image mainItem, bookImage;
    public TextMeshProUGUI bookData, timerTxt;
    public Button mixButton;
    public Text countdownTxt;
    public GameObject panelCountDown, panelOut;
    public TextMeshProUGUI outTxt;
    public GameObject plate1, plate2;
    public MockData mockData;
    bool isMixMainEnded = false;
    public ItemDragHandle[] itemsMainMix, itemsSubMix;

  Global global;
    Collider bowlCollider;
    int currentTimer;
    //StructData data;
    void Start() {
        global = Global.ins;
        bowlCollider = bowl.GetComponent<Collider>();
        currentTimer = (int)global.gameTime;
        timerTxt.text = currentTimer+"";
        /*
        data = global.listDatas[global.itemSelector];
        for(int i = 0; i < ingradiants.Length; i++){
            ingradiants[i].sprite = data.img[i+1];
        }
        mainItem.sprite = bookImage.sprite = data.img[0];
        bookData.text = data.destData[0];
        */
        //    RandomItemDrags();
        Invoke("OnTimelineCompleted", 9f);

        SetMainSubMix();
    }

    private void SetMainSubMix() {
        for (int i = 0; i < itemsMainMix.Length; i++) {
            itemsMainMix[i].SetData(
                mockData.datas[i].sp, mockData.datas[i].data, mockData.datas[i].index);
            itemsSubMix[i].SetData(
               mockData.additionals[i].sp, mockData.additionals[i].data, mockData.additionals[i].index);

            itemsMainMix[i].OnDrop += OnDragItemIsDrop;
            itemsSubMix[i].OnDrop += OnDragItemIsDrop;
        }
    }


    void OnTimelineCompleted() {
        count = 3;
        UpdateCountText();
    }

    void StartGamePlay() {
        panelCountDown.SetActive(false);
        StartTimer();
        MovePlate(1);
    }

    void MovePlate(int id) {
        if(id == 1) {
            plate1.transform.DOLocalMoveY(-330, .5f);
        } else {
            plate1.transform.DOLocalMoveY(-1000, .5f).OnComplete(()=>
                plate2.transform.DOLocalMoveY(-330, .5f));
        }
    }

    int GetRandomNumByException(int[] excNum, int maximum) {
        int r = Mathf.FloorToInt(Random.Range(0, maximum));
        for(int i = 0; i < excNum.Length; i++) {
            if(excNum[i] == r) {
                r = GetRandomNumByException(excNum, maximum);
            }
        }
        return r;
    }

    void OnDragItemIsDrop(ItemDragHandle iDrag) {
        if (isMixMainEnded) {
            if (iDrag.GetCollider().bounds.Intersects(bowlCollider.bounds)) {
                for (int i = 0; i < bowDataSlot.Length; i++) {
                    if (iDrag == bowDataSlot[i]) {
                        iDrag.transform.position = bowlLockSlot[i].transform.position;
                        CheckItemIsFull();
                        return;
                    } else if (bowDataSlot[i] == null) {
                        iDrag.transform.position = bowlLockSlot[i].transform.position;
                        bowDataSlot[i] = iDrag;
                        CheckItemIsFull();
                        return;
                    }
                }
                //print("full");
                iDrag.ResetPosition();
            } else {
                for (int i = 0; i < bowDataSlot.Length; i++) {
                    if (iDrag == bowDataSlot[i]) {
                        bowDataSlot[i] = null;
                        break;
                    }
                }
                CheckItemIsFull();
                iDrag.ResetPosition();
            }
        } else {
            if (iDrag.GetCollider().bounds.Intersects(bowlCollider.bounds)) {
                if(bowDataSlot[iDrag.GetIndexOfMixer] == null) {
                    iDrag.transform.position = bowlLockSlot[iDrag.GetIndexOfMixer].transform.position;
                } else {
                    iDrag.transform.position = bowlLockSlot[iDrag.GetIndexOfMixer].transform.position;
                    bowDataSlot[iDrag.GetIndexOfMixer].ResetPosition();
                }
                bowDataSlot[iDrag.GetIndexOfMixer] = iDrag;
                CheckItemIsFull();
            } else {
                if(bowDataSlot[iDrag.GetIndexOfMixer] == iDrag) {
                    bowDataSlot[iDrag.GetIndexOfMixer] = null;
                }
                CheckItemIsFull();
                iDrag.ResetPosition();
            }
        }
    }

    void CheckItemIsFull() {
        for(int i = 0; i < bowDataSlot.Length; i++){
            if(bowDataSlot[i] == null){
                ShowMixButton(false);
                return;
            } 
        }
        ShowMixButton(true);
    }

    void ShowMixButton(bool isShow) {
        if(isShow){
            mixButton.transform.DOLocalMoveY(460, .15f).OnComplete(()=>
                mixButton.gameObject.SetActive(isShow));
        }else{
            mixButton.transform.DOLocalMoveY(630, .15f).OnComplete(()=>
                mixButton.gameObject.SetActive(isShow));
        }
    }
   
    public void ShowBookData(Sprite spritep, string s) {
        bookImage.sprite = spritep;
        bookData.text = s;
    }

    void OnMixMain() {

    }

    public void OnMix() {
        ShowMixButton(false);
        bool[] isCorrect = new bool[3];
        for(int i = 0; i < bowDataSlot.Length; i++) {
            if (!isMixMainEnded) {
                global.mainSpr[i] = bowDataSlot[i].ItemImage;
                global.mainData[i] = bowDataSlot[i].ItemData;
                bowDataSlot[i].ResetPosition();
                bowDataSlot[i] = null;
            } else {
                global.subSpr[i] = bowDataSlot[i].ItemImage;
                global.subData[i] = bowDataSlot[i].ItemData;
            }/*
            for (int j = 0; j < 3; j++){
                isCorrect[i] = false;
                if(ingradiants[i].sprite == bowDataSlot[j].ItemImage){
                    print("true");
                    isCorrect[i] = true;
                    break;
                }
            }*/
        }
      
        /*
        foreach(bool b in isCorrect){
            if(b==false){
                global.PlayerResult = false;
                 break;
            }
        }*/
        if (isMixMainEnded) {
            global.PlayerResult = true;
            OnEndScene(false);
        } else {
            isMixMainEnded = true;
            MovePlate(2);
        }
    }

    void StartTimer() {
        StartCoroutine(UpdateTimer());
    }

    IEnumerator UpdateTimer() {
        if(currentTimer == 0) { //Time up mission failed.
            global.PlayerResult = false;
            OnEndScene(true);
            yield break;
        }
        yield return new WaitForSeconds(1f);
        currentTimer--;
        timerTxt.text = TwoDigit(currentTimer);
        StartCoroutine(UpdateTimer());
    }

    string TwoDigit(int num) {
        return (num<10)? "0"+num: ""+num;
    }
    
    void OnEndScene(bool isTimeUp) {
        print("Result >>> " +global.PlayerResult);
        outTxt.gameObject.SetActive(isTimeUp);
        panelOut.SetActive(true);
        panelOut.GetComponent<Image>().DOFade(1, 1.5f).OnComplete(()=>
             global.LoadSceneAt(6));
    }

    #region Text Countdown
    int textSize;
    int count;
    void UpdateCountText() {
        textSize = 400;            
        if (count > 0) {
            countdownTxt.text = count.ToString();
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(UpdateCountText);
        } else {
            countdownTxt.text = "START";
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(StartGamePlay);
        }
        count--;    
    }

    void UpdateTextSize() {
        countdownTxt.fontSize = textSize;
    }
#endregion

}
